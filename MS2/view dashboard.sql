CREATE OR REPLACE VIEW "dashboard"."v_f_critical_stock"
AS
SELECT
	"f_critical_stock"."critical_stock_sk",
	"f_critical_stock"."spbu_sk",
	"d_spbu"."spbu_code",
	"d_spbu"."spbu_desc",
	"d_spbu"."spbu_status_sk",
	"d_spbu"."spbu_tbbm_sk",
	"d_spbu"."spbu_city_sk",
	"d_spbu"."spbu_province_sk",
	"f_critical_stock"."period_sk",
	"d_period"."year",
	"d_period"."month_code",
	"d_period"."month_short_name",
	"d_period"."month_name",
	"d_period"."date",
	"f_critical_stock"."product_sk",
	"d_product"."material_code",
	"d_product"."material_name",
	"d_product"."product_code",
	"d_product"."product_name",
	"f_critical_stock"."shift_sk",
	"d_shift"."shift_code",
	"d_shift"."shift_name",
	"f_critical_stock"."critical_category_sk",
	"d_critical_category"."critical_category_code",
	"d_critical_category"."critical_category_name",
	"f_critical_stock"."critical_cond_freq" 
FROM
	"dashboard"."f_critical_stock" "f_critical_stock" 
		LEFT JOIN "dashboard"."d_spbu" "d_spbu" 
		ON "f_critical_stock"."spbu_sk" = "d_spbu"."spbu_sk" 
			LEFT JOIN "dashboard"."d_period" "d_period" 
			ON "f_critical_stock"."period_sk" = "d_period"."period_sk" 
				LEFT JOIN "dashboard"."d_product" "d_product" 
				ON "f_critical_stock"."product_sk" = "d_product"."product_sk" 
					LEFT  JOIN "dashboard"."d_shift" "d_shift" 
					ON "f_critical_stock"."shift_sk" = "d_shift"."shift_sk" 
						LEFT JOIN "dashboard"."d_critical_category" 
						"d_critical_category" 
						ON "f_critical_stock"."critical_category_sk" = 
						"d_critical_category"."critical_category_sk"
GO


CREATE OR REPLACE VIEW "dashboard"."v_f_kpi_delivery"
AS
SELECT
	"f_kpi_delivery"."kpi_delivery_sk",
	"f_kpi_delivery"."deliv_numb",
	"f_kpi_delivery"."lo_period_sk",
	"lo_d_period"."year" "lo_year",
	"lo_d_period"."month_code" "lo_month_code",
	"lo_d_period"."month_short_name" "lo_month_short_name",
	"lo_d_period"."month_name" "lo_month_name",
	"lo_d_period"."date" "lo_date",
	"f_kpi_delivery"."product_sk",
	"d_product"."material_code",
	"d_product"."material_name",
	"d_product"."product_code",
	"d_product"."product_name",
	"f_kpi_delivery"."spbu_sk",
	"d_spbu"."spbu_code",
	"d_spbu"."spbu_desc",
	"f_kpi_delivery"."lo_shift_sk",
	"lo_d_shift"."shift_code" "lo_shift_code",
	"lo_d_shift"."shift_name" "lo_shift_name",
	"f_kpi_delivery"."lo_quantity",
	"f_kpi_delivery"."gi_period_sk",
	"gi_d_period"."year" "gi_year", 
	"gi_d_period"."month_code" "gi_month_code", 
	"gi_d_period"."month_short_name" "gi_month_short_name", 
	"gi_d_period"."month_name" "gi_month_name", 
	"gi_d_period"."date" "gi_date", 
	"f_kpi_delivery"."gi_shift_sk",
	"gi_d_shift"."shift_code" "gi_shift_code",
	"gi_d_shift"."shift_name" "gi_shift_name",
	"f_kpi_delivery"."gi_quantity",
	"f_kpi_delivery"."gi_status_sk",
	"d_gi_status"."gi_stat_code",
	"d_gi_status"."gi_stat_name" 
FROM
	"dashboard"."f_kpi_delivery" "f_kpi_delivery" 
		LEFT OUTER JOIN "dashboard"."d_period" "lo_d_period" 
		ON "f_kpi_delivery"."lo_period_sk" = "lo_d_period"."period_sk" 
			LEFT OUTER JOIN "dashboard"."d_product" "d_product" 
			ON "f_kpi_delivery"."product_sk" = "d_product"."product_sk" 
				LEFT OUTER JOIN "dashboard"."d_spbu" "d_spbu" 
				ON "f_kpi_delivery"."spbu_sk" = "d_spbu"."spbu_sk" 
					LEFT OUTER JOIN "dashboard"."d_shift" "lo_d_shift" 
					ON "f_kpi_delivery"."lo_shift_sk" = "lo_d_shift"."shift_sk" 
						LEFT OUTER JOIN "dashboard"."d_gi_status" "d_gi_status" 
						ON "f_kpi_delivery"."gi_status_sk" = "d_gi_status". 
						"gi_stat_sk" 
							INNER JOIN "dashboard"."d_shift" "gi_d_shift" 
							ON "f_kpi_delivery"."gi_shift_sk" = "gi_d_shift". 
							"shift_sk" 
								INNER JOIN "dashboard"."d_period" "gi_d_period" 
								ON "f_kpi_delivery"."gi_period_sk" = 
								"gi_d_period"."period_sk"