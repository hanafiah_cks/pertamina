create table opt.stg_t_basehist as
SELECT
    "t_spbu"."id_spbu"::int id_spbu,
    "t_product"."product_id"::int id_product,
    date_trunc('day', dd):: timestamp tanggal
FROM
    generate_series( '2018-08-02'::timestamp, '2018-10-31'::timestamp, '1 day'::interval) dd
    CROSS JOIN "opt"."t_product" "t_product" 
    CROSS JOIN "opt"."t_spbu" "t_spbu" 
WHERE
	product_id NOT IN(10,11,12,	13)

go

SELECT * FROM opt.t_gi
where gi_date='2018-08-01'::date
go
SELECT upper(to_char('2018-08-01'::timestamp,'day'))
go
SELECT upper(trim(ac_date)),*,24*sales_est FROM opt.t_sales_est limit 1000
go
select *,tanggal::date from opt.stg_t_basehist
go
select * from opt.stg_t_basehist a 
left join (SELECT id_spbu,id_product,gi_date,sum(quantity) FROM opt.t_gi
group by id_spbu,id_product,gi_date
) b on a.id_spbu =b.id_spbu and a.id_product =b.id_product and a.tanggal::date = date(b.gi_date +1)
left join opt.t_sales_est c on a.id_spbu =c.id_spbu and a.id_product =c.id_product 
and upper(trim(c.ac_date))=upper(to_char(a.tanggal,'day'))
where a.tanggal::date='2018-08-02'::date
order by tanggal
limit 100000
go

select upper(trim(to_char(a.tanggal,'day'))),upper(trim(c.ac_date)),* from opt.stg_t_basehist a 
left join opt.t_sales_est c on a.id_spbu =c.id_spbu and a.id_product =c.id_product 
and upper(trim(to_char(a.tanggal,'day')))=upper(trim(c.ac_date))
where a.tanggal::date='2018-08-02'::date
limit 5000
go
SELECT * FROM opt.t_sales_est WHERE
id_spbu= 2505
go
select * from opt.t_initial_stock where stock =0
go
SELECT distinct(id_product) FROM opt.t_sales_est
go
select floor(random()* (99-96 + 1) + 96)/100 test from generate_series(1,100)
go