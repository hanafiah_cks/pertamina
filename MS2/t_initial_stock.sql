INSERT INTO opt.t_historical_stock
SELECT
x.id_spbu,x.product_id,coalesce(y.datetime_stock,'2018-08-01'::timestamp) date,coalesce(y.stock,0) stock	 
FROM
	(	SELECT
			"t_spbu"."id_spbu",
			"t_product"."product_id" 
		FROM
			"opt"."t_product" "t_product" 
			CROSS JOIN "opt"."t_spbu" "t_spbu" 
		WHERE
			(product_id NOT IN(10,
			11,
			12,
			13))) "x" 
		LEFT  JOIN (	select * from (SELECT
								"t_stock"."id_spbu",
								"t_stock"."id_product",
								"t_stock"."datetime_stock",
								"t_stock"."stock",
								row_number() over(partition by id_spbu,id_product,datetime_stock::date order by datetime_stock) rn
							FROM
								"opt"."t_stock" "t_stock" 
							WHERE
								(datetime_stock::date = DATE('2018-08-01')))as j where rn =1) "y" 
		ON "x"."id_spbu" = "y"."id_spbu" AND
		"x"."product_id" = "y"."id_product"
		
go

SELECT * from ( select
								"t_stock"."id_spbu",
								"t_stock"."id_product",
								"t_stock"."datetime_stock",
								"t_stock"."stock",
								row_number() over(partition by id_spbu,id_product,datetime_stock::date order by datetime_stock) rn
							FROM
								"opt"."t_stock" "t_stock" ) as j where rn=1
				
go
SELECT date_trunc('day', dd):: timestamp tanggal
FROM generate_series
        ( '2018-08-02'::timestamp 
        , '2018-10-31'::timestamp
        , '1 day'::interval) dd

        ;
go

SELECT
			"t_spbu"."id_spbu",
			"t_product"."product_id",
			 date_trunc('day', dd):: timestamp tanggal
		FROM
			 generate_series
        ( '2018-08-02'::timestamp 
        , '2018-10-31'::timestamp
        , '1 day'::interval) dd
    	cross join "opt"."t_product" "t_product" 
	    CROSS JOIN "opt"."t_spbu" "t_spbu" 
		WHERE
			(product_id NOT IN(10,
			11,
			12,
			13))